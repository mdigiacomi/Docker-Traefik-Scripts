#!/bin/bash
sudo docker run -d --name traefik --restart=always -p 8080:8080 -p 80:80 -p 443:443 -v /etc/traefik/traefik.toml:/etc/traefik/traefik.toml -v /etc/traefik/acme.json:/etc/traefik/acme.json -v /var/run/docker.sock:/var/run/docker.sock traefik
