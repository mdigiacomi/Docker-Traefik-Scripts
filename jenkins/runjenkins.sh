#!/bin/bash
sudo docker run --privileged -d --name jenkins -l traefik.frontend.rule=Host:jenkins.hipsterconsulting.com -v /home/ubuntu/docker-storage/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:latest
